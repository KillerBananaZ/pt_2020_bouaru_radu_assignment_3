package bll;

import java.util.List;

import com.itextpdf.text.Document;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.OrderItemDAO;
import model.Client;
import model.OrderItem;
import model.Orders;
import presentation.PDFWriter;
/**
 * Clasa ClientBLL ofera metodele CRUD specifice pentru un client
 */
public class ClientBLL {
	/**
	 * Atributele acestei clase vor fi prelua Data Access Objects necesare pentru a 
	 * prelucra clientii cu success
	 */
	private ClientDAO clientDAO;
	private OrderItemDAO orderItemDAO;
	private OrderDAO orderDAO;
	/**
	 * Constructorul clasei ClientBLL, care instantiaza DAO-urile definite prin atribute
	 */
	public ClientBLL() {
		clientDAO = new ClientDAO();
		orderItemDAO = new OrderItemDAO();
		orderDAO = new OrderDAO();
	}
	/**
	 * Metoda insertClient va insera in baza de date un client, prin clientDAO
	 * @param client Clientul care se insereaza
	 */
	public void insertClient(Client client) {
		clientDAO.insert(client);
	}
	/**
	 * Metoda selectClients va prelua toti clientii din baza de date, prin clientDAO
	 */
	public void selectClients() {
		clientDAO.select();
	}
	/**
	 * Metoda findID va cauta ID-ul unui client in baza de date, prin clientDAO
	 * @param client Clientul a carui ID va fi cautat
	 * @return ID-ul clientului cautat
	 */
	public int findID(Client client)
	{
		return clientDAO.findID(client);
	}
	/**
	 * Metoda deleteClient va sterge un client din baza de date, asigurandu-ne ca toate
	 * aparitiile acestui client in sistem se sterg odata cu acesta: se sterg in acelasi
	 * timp toate comenzile pe care le-a facut acesta
	 * @param client Clientul care va fi sters din baza de date
	 */
	public void deleteClient(Client client)
	{
		List<OrderItem> orderItemList = orderItemDAO.findAll();
		List<Orders> orderList = orderDAO.findAll();
		Orders toBeDeleted = null;
		int clientID = clientDAO.findID(client);
		int orderID = -1;
		for(Orders o: orderList)
		{
			if(o.getClientID() == clientID)
			{
				orderID = orderDAO.findID(o);
				toBeDeleted = o;
				break;
			}
		}
		if(orderID != -1)
		{
			for(OrderItem oi: orderItemList)
			{
				if(oi.getOrderID() == orderID)
					orderItemDAO.delete(oi);
			}
			orderDAO.delete(toBeDeleted);
		}
		clientDAO.delete(client);
	}
	/**
	 * Metoda generateClientReport va genera raportul tuturor clientilor prezenti in baza de date,
	 * sub forma tabulara, intr-un fisier PDF extern
	 */
	public void generateClientReport() {
		Document document = new Document();
		PDFWriter pdfWriter = new PDFWriter(document, "Client");
		document.open();
		pdfWriter.generateClients(document, clientDAO);
		document.close();
	}
}
