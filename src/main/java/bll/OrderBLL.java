package bll;

import java.util.List;

import com.itextpdf.text.Document;

import dao.ClientDAO;
import dao.OrderDAO;
import dao.OrderItemDAO;
import model.Client;
import model.OrderItem;
import model.Orders;
import model.Product;
import presentation.PDFWriter;
/**
 * Clasa OrderBLL ofera metodele CRUD specifice pentru un client
 */
public class OrderBLL {
	/**
	 * Atributele acestei clase vor fi prelua Data Access Objects necesare pentru a 
	 * prelucra clientii cu success
	 */
	private OrderDAO orderDAO;
	private OrderItemDAO orderItemDAO;
	private ClientDAO clientDAO;
	private ProductBLL productBLL;
	private ClientBLL clientBLL;
	/**
	 * Constructorul clasei OrderBLL, care instantiaza DAO-urile definite prin atribute
	 */
	public OrderBLL() {
		orderDAO = new OrderDAO();
		orderItemDAO = new OrderItemDAO();
		clientDAO = new ClientDAO();
		productBLL = new ProductBLL();
		clientBLL = new ClientBLL();
	}
/**
 * Metoda insertOrder va introduce o noua comanda in baza de date, tinand cont de diverse
 * evenimente care se pot intampla:
 * 		- daca in baza de date nu exista produsul care se doreste a fi comandat, comanda nu are loc
 * 		- daca stocul produsului din baza de date este mai mic decat cantitatea care se doreste
 * 			a fi comandata, comanda nu are loc
 * 		- daca clientul care face o comanda are deja o comanda anterioara, produsele comandate se
 * 			vor adauga la comanda anterioara, si nu se va mai deschide o comanda noua
 * @param client Clientul care face o comanda
 * @param product Produsul pe care un client il comanda
 * @param productQuantity Cantitatea de produs comandata
 */
	public void insertOrder(Client client, Product product, int productQuantity) {
		Orders order = new Orders(clientDAO.findID(client));
		int orderID = orderDAO.findID(order);
		int productID = productBLL.findID(product);
		if(productID != -1)
		{
			Product existingProduct = productBLL.getProduct(productID);
			if (orderID == -1) {
				if (existingProduct.getQuantity() > productQuantity) {
					orderDAO.insert(order);
					orderID = orderDAO.findID(order);
					OrderItem orderItem = new OrderItem(productID, productQuantity, orderID);
					orderItemDAO.insert(orderItem);
					productBLL.updateProduct(existingProduct, productQuantity);
					generateBill(client);
				}
			} else {
				OrderItem orderItem = new OrderItem(productID, productQuantity, orderID);
				if (productBLL.updateProduct(existingProduct, productQuantity) == 1) {
					int orderItemID = orderItemDAO.findID(orderItem);
					if (orderItemID == -1) {
						{
							orderItemDAO.insert(orderItem);
							generateBill(client);
						}					
					} else {
						OrderItem existingOrderItem = orderItemDAO.findById(orderItemID);
						existingOrderItem.setProductQuantity(productQuantity + existingOrderItem.getProductQuantity());
						orderItemDAO.update(existingOrderItem, String.valueOf(orderItemID));
						generateBill(client);
					}
				}
			}
		}
	}
/**
 * Metoda findOrderID va gasi comanda specifica unui anumit client
 * @param client Clientul a carui numar de comanda se gaseste
 * @return Numarul comenzii 
 */
	public int findOrderID(Client client) {
		return orderDAO.findID(new Orders(clientDAO.findID(client)));
	}
	/**
	 * Metoda generateBill() va genera o factura pentru un anumit client, atunci cand acesta
	 * face o comanda cu success
	 * @param client Clientul pentru care se genereaza o factura
	 */
	public void generateBill(Client client)
	{
		Document document = new Document();		
		PDFWriter pdfWriter = new PDFWriter(document, "Client " + client.getName());
		document.open();
		pdfWriter.generateBillHeader(document, client, clientBLL, this, productBLL);
		pdfWriter.generateOrderItems(document, client, orderItemDAO, this, productBLL);
		document.close();
	}
	/**
	 * Metoda generateOrderReport va genera un raport pentru toate facturile prezente in baza de date
	 */
	public void generateOrderReport()
	{
		Document document = new Document();
		PDFWriter pdfWriter = new PDFWriter(document, "Order");
		document.open();
		pdfWriter.addText(document, "All the bills in the system:\n\n\n");
		List<Client> clientList = clientDAO.findAll();
		for(Client c: clientList)
		{
			if(findOrderID(c) != -1)
			{
				pdfWriter.generateBillHeader(document, c, clientBLL, this, productBLL);
				pdfWriter.generateOrderItems(document, c, orderItemDAO, this, productBLL);
			}
		}
		document.close();
	}

}
