package bll;

import java.util.List;

import com.itextpdf.text.Document;

import dao.OrderItemDAO;
import dao.ProductDAO;
import model.OrderItem;
import model.Product;
import presentation.PDFWriter;
/**
 * Clasa ProductBLL ofera metodele CRUD specifice pentru un client
 */
public class ProductBLL {
	/**
	 * Atributele acestei clase vor fi prelua Data Access Objects necesare pentru a 
	 * prelucra clientii cu success
	 */
	private ProductDAO productDAO;
	private OrderItemDAO orderItemDAO;
	/**
	 * Constructorul clasei ClientBLL, care instantiaza DAO-urile definite prin atribute
	 */
	public ProductBLL() {
		this.productDAO = new ProductDAO();
		this.orderItemDAO = new OrderItemDAO();
	}
	/**
	 * Metoda insertProduct va insera in baza de date un produs, prin productDAO
	 * In cazul in care produsul inserat exista deja in stoc, se va updata stocul
	 * @param product Produsul care se insereaza
	 */
	public void insertProduct(Product product){
		int productID = productDAO.findID(product);
		if(productID != -1)
		{
			Product updatedProduct = productDAO.findById(productID);
			updatedProduct.setQuantity(updatedProduct.getQuantity() + product.getQuantity());
			productDAO.update(updatedProduct, updatedProduct.getProductName());
		}
		else
		{
			productDAO.insert(product);
		}
	}
	/**
	 * Metoda selectProducts va prelua toate produsele din baza de date, prin productDAO
	 */
	/*	public void selectProduct() {
		productDAO.select();
	}*/
	/**
	 * Metoda findID va cauta ID-ul unui produs in baza de date, prin productDAO
	 * @param product Produsul a carui ID va fi cautat
	 * @return ID-ul produsului cautat
	 */
	public int findID(Product product) {
		return productDAO.findID(product);
	}
	/**
	 * Metoda getProduct va cauta produsul cu un anumit ID in baza de date
	 * @param id ID-ul produsului care se cauta
	 * @return Produsul din baza de date
	 */
	public Product getProduct(int id) {
		return productDAO.findById(id);
	}
	/**
	 * Metoda deleteProduct va sterge un produs din baza de date, asigurandu-ne ca toate
	 * aparitiile acestui produs in sistem se sterg odata cu acesta: se sterg in acelasi
	 * timp toate comenzile in care a fost comandat acest produs
	 * @param product Product care va fi sters din baza de date
	 */
	public void deleteProduct(Product product)
	{
		List<OrderItem> orderItemList = orderItemDAO.findAll();
		int productID = productDAO.findID(product);
		for(OrderItem oi: orderItemList)
		{
			if(oi.getProductID() == productID)
			{
				orderItemDAO.delete(oi);
				break;
			}
		}
		productDAO.delete(product);		
	}
/**
 * Metoda updateProduct va modifica stocul curent al unui produs, cand acesta se comanda
 * @param product Produsul comandat
 * @param productOrderedQuantity Cantitatea produsului comandat
 * @return -1 daca cantitatea produsului nu este suficienta, 1 altfel
 */
	public int updateProduct(Product product, int productOrderedQuantity) {
		if (product.getQuantity() < productOrderedQuantity)
		{
			System.out.println("Insufficient quantity");
			return -1;
		}			
		else {
			Product updatedProduct = new Product(product.getProductName(),
					product.getQuantity() - productOrderedQuantity, product.getPrice());
			productDAO.update(updatedProduct, updatedProduct.getProductName());
			return 1;
		}
	}
	/**
	 * Metoda generateProductReport va genera un raport cu privire la toate produsele existente in baza de date
	 */
	public void generateProductReport() {
		Document document = new Document();
		PDFWriter pdfWriter = new PDFWriter(document, "Product");
		document.open();
		pdfWriter.generateProducts(document, productDAO);
		document.close();
	}
}
