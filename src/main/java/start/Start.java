package start;

import java.sql.SQLException;
import java.util.logging.Logger;

import presentation.ParseFile;

public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {
		if (args.length == 0)
			System.out.println("No arguments!");
		else {
			@SuppressWarnings("unused")
			ParseFile executer = new ParseFile(args[0]);
		}

	}
}
