package presentation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import dao.ClientDAO;
import dao.OrderItemDAO;
import dao.ProductDAO;
import model.Client;
import model.OrderItem;
import model.Product;
/**
 * Clasa care va scrie in fisierul PDF datele corespunzatoare rapoartelor
 */
public class PDFWriter {
/**
 * Constructorul clasei PDFWriter
 * @param document Documentul in care se vor scrie datele
 * @param pdfName Numele fisierului PDF, asa cum va aparea pe disc
 */
	public PDFWriter(Document document, String pdfName) {
		try {
			PdfWriter.getInstance(document, new FileOutputStream(pdfName + " report.pdf"));
		} catch (FileNotFoundException | DocumentException e) {
			e.printStackTrace();
		}
	}
/**
 * Metoda care adauga un cap de tabel
 * @param columnTitle Titlul coloanei din capul de tabel
 * @param table Tabelul care va primi capul de tabel
 */
	public void addTableHeader(String columnTitle, PdfPTable table) {

		PdfPCell header = new PdfPCell();
		header.setBackgroundColor(BaseColor.LIGHT_GRAY);
		header.setBorderWidth(2);
		header.setBorderColor(BaseColor.ORANGE);
		header.setHorizontalAlignment(Element.ALIGN_CENTER);
		header.setVerticalAlignment(Element.ALIGN_MIDDLE);
		header.setPhrase(new Phrase(columnTitle));
		table.addCell(header);

	}
/**
 * Metoda care adauga un rand nou cu un anumit continut
 * @param content Continutul care urmeaza a fi adaugat
 * @param table Tabelul care va primi randul generat
 */
	public void addRows(String content, PdfPTable table) {
		table.addCell(content);
	}
/**
 * Metoda care va genera formatul de fisier PDF specific unei facturi
 * @param document Documentul in care se scrie
 * @param client Clientul pentru care se genereaza factura
 * @param clientBLL BLL-ul clientului pentru a accesa metoda sa de READ
 * @param orderBLL BLL-ul comenzii pentru a accesa metoda sa de READ
 * @param productBLL BLL-ul produsului pentru a accesa metoda sa de READ
 */
	public void generateBillHeader(Document document, Client client, ClientBLL clientBLL, OrderBLL orderBLL,
			ProductBLL productBLL) {
		try {
			OrderItemDAO orderItemDAO = new OrderItemDAO();

			StringBuilder stringHeader = new StringBuilder();
			int orderID = orderBLL.findOrderID(client);
			double totalPrice = 0;

			List<OrderItem> orderItemList = orderItemDAO.findAll();
			for (OrderItem oi : orderItemList) {
				if (oi.getOrderID() == orderID) {
					Product product = productBLL.getProduct(oi.getProductID());
					totalPrice += oi.getProductQuantity() * product.getPrice();
				}
			}
			stringHeader.append(" client #" + String.valueOf(clientBLL.findID(client)) + ":    " + client.getName());
			document.add(new Paragraph("Auto generated bill for" + stringHeader.toString() + "\n\n"));
			document.add(new Paragraph("Bill total: " + String.valueOf(totalPrice) + "\n\n"));

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Metoda care genereaza un tabel cu toate produsele aferente unei comenzi
	 * @param document Documentul in care se scrie
	 * @param client Clientul pentru care se genereaza factura
	 * @param orderItemDAO DAO-ul pentru produsele aferente unei facturi, pentru a accesa metoda de READ
	 * @param orderBLL BLL-ul comenzii pentru a accesa metoda sa de READ
	 * @param productBLL BLL-ul produsului pentru a accesa metoda sa de READ
	 */

	public void generateOrderItems(Document document, Client client, OrderItemDAO orderItemDAO, OrderBLL orderBLL,
			ProductBLL productBLL) {
		PdfPTable table = new PdfPTable(3);
		try {
			document.add(new Paragraph("Ordered items\n\n"));
			addTableHeader("Product name", table);
			addTableHeader("Product quantity", table);
			addTableHeader("Product price", table);

			int orderID = orderBLL.findOrderID(client);
			List<OrderItem> orderItemList = orderItemDAO.findAll();

			for (OrderItem oi : orderItemList) {
				if (oi.getOrderID() == orderID) {
					Product product = productBLL.getProduct(oi.getProductID());
					addRows(product.getProductName(), table);
					addRows(String.valueOf(oi.getProductQuantity()), table);
					addRows(String.valueOf(product.getPrice()), table);
				}
			}
			document.add(table);
			document.add(new Paragraph("\n\n\n"));
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Metoda care adauga un anumit paragraf intr-un document
	 * @param document Documentul in care se scrie
	 * @param text Textul care se adauga in PDF
	 */
	public void addText(Document document, String text)
	{
		try {
			document.add(new Paragraph(text));
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Metoda care genereaza un raport cu privire la toti clientii din baza de date
	 * @param document Documentul in care se scrie
	 * @param clientDAO DAO-ul de client, din care se apeleaza metoda de READ
	 */
	public void generateClients(Document document, ClientDAO clientDAO)
	{
		PdfPTable table = new PdfPTable(3);
		try {
			document.add(new Paragraph("Clients\n\n"));
			addTableHeader("Client ID", table);
			addTableHeader("Client name", table);
			addTableHeader("Client address", table);
			
			List<Client> clientList = clientDAO.findAll();
			for(Client c: clientList)
			{
				int clientID = clientDAO.findID(c);
				addRows(String.valueOf(clientID), table);
				addRows(c.getName(), table);
				addRows(c.getAddress(), table);
			}
			document.add(table);			
		}
		catch(DocumentException e)
		{
			e.printStackTrace();
		}
	}
	/**
	 * Metoda care genereaza un raport cu privire la toate produsele din baza de date
	 * @param document Documentul in care se scrie
	 * @param productDAO DAO-ul de produs, din care se apeleaza metoda de READ
	 */
	public void generateProducts(Document document, ProductDAO productDAO)
	{
		PdfPTable table = new PdfPTable(4);
		try {
			document.add(new Paragraph("Products\n\n"));
			addTableHeader("Product ID", table);
			addTableHeader("Product name", table);
			addTableHeader("Product quantity", table);
			addTableHeader("Product price", table);
			
			List<Product> productList = productDAO.findAll();
			for(Product p: productList)
			{
				int productID = productDAO.findID(p);
				addRows(String.valueOf(productID), table);
				addRows(p.getProductName(), table);
				addRows(String.valueOf(p.getQuantity()), table);
				addRows(String.valueOf(p.getPrice()), table);
			}
			document.add(table);			
		}
		catch(DocumentException e)
		{
			e.printStackTrace();
		}
	}
}
