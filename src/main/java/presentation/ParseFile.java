package presentation;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Product;
/**
 * Clasa care se ocupa de prelucrarea fisierului de intrare si de trimiterea datelor spre
 * clasele corespunzatoare
 */
public class ParseFile {
/**
 * Constructorul clasei ParseFile
 * @param filePath Calea catre fisierul de intrare care contine comenzile 
 * Aici se prelucreza datele, in functie de tipul interogarii. Se foloseste un
 * StringTokenizer care imparte o linie citita din fisier in parametrii specifici
 * pentru interogarile SQL 
 */
	public ParseFile(String filePath) {
		try {
			ClientBLL clientBLL = new ClientBLL();
			ProductBLL productBLL = new ProductBLL();
			OrderBLL orderBLL = new OrderBLL();

			File inputFilePath = new File(filePath);
			Scanner fileReader = new Scanner(inputFilePath);
			while (fileReader.hasNextLine()) {
				String data = fileReader.nextLine();
				StringTokenizer splitInputString = new StringTokenizer(data, ",:");
				ArrayList<String> inputString = new ArrayList<String>();

				while (splitInputString.hasMoreTokens()) {
					String query = splitInputString.nextToken().toString().trim();
					if (query.contains(" ")) {
						inputString.add(query.substring(0, query.indexOf(" ")));
						inputString.add(query.substring(query.indexOf(" ") + 1, query.length()));

					} else
						inputString.add(query);

				}

				if (inputString.get(0).equalsIgnoreCase("insert")) {
					if (inputString.get(1).equalsIgnoreCase("client")) {
						Client newClient = new Client(inputString.get(2) + " " + inputString.get(3),
								inputString.get(4));
						clientBLL.insertClient(newClient);
					}
					if (inputString.get(1).equalsIgnoreCase("product")) {
						Product newProduct = new Product(inputString.get(2), Integer.parseInt(inputString.get(3)),
								Double.parseDouble(inputString.get(4)));
						productBLL.insertProduct(newProduct);
					}
				}
				if (inputString.get(0).equalsIgnoreCase("delete")) {
					if (inputString.get(1).equalsIgnoreCase("client")) {
						Client toBeDeleted = new Client(inputString.get(2) + " " + inputString.get(3),
								inputString.get(4));
						clientBLL.deleteClient(toBeDeleted);
					}
					if (inputString.get(1).equalsIgnoreCase("product")) {
						Product toBeDeleted = new Product(inputString.get(2), 0, 0);
						productBLL.deleteProduct(toBeDeleted);
					}
				}
				if (inputString.get(0).equalsIgnoreCase("order")) {
					Client client = new Client(inputString.get(1) + " " + inputString.get(2), " ");
					Product product = new Product(inputString.get(3), 0, 0);
					orderBLL.insertOrder(client, product, Integer.parseInt(inputString.get(4)));
				}
				if (inputString.get(0).equalsIgnoreCase("report")) {
					switch (inputString.get(1)) {
					case "client":
						clientBLL.generateClientReport();
						break;
					case "product":
						productBLL.generateProductReport();
						break;
					case "order":
						orderBLL.generateOrderReport();
						break;
					}
				}
			}
			fileReader.close();
		} catch (FileNotFoundException ex) {
			System.out.println("Error! Input file not found");
		}

	}

}
